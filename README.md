# README

**提供中文版以及英文版，請選擇您熟悉的語言進行閱讀**

**Available in both Chinese and English. Please select the language you are comfortable with.**

**選擇語言 / Choose Your Language**

- [中文版](README.md#中文版)
- [English Version](README.md#english-version)

# 中文版

# 專案介紹

## 描述

Music Share 是一個音樂分享平台，旨在讓使用者能夠輕鬆地上傳、分享和串流播放音樂。

這邊為此專案之 Cloud infrastructure repository, 詳細專案介紹請查看後端 Repository README。

- **後端 Repository**：[https://gitlab.com/music-share66/backend.git](https://gitlab.com/music-share66/backend.git)

## Demo 網址

可以在以下網址查看 Music Share：

[https://musicshare.liaojourney.com](https://musicshare.liaojourney.com/)

**注意事項**：

- 由於沒有製作響應式網頁設計 (RWD)，請以 1500x1180 以上的解析度大小顯示器開啟，以獲得良好的瀏覽體驗。
- 開放時間：09:00 GMT+8 - 18:00 GMT+8

## Demo 影片

可以在以下網址查看 Music Share 平台的 Demo 影片：

[https://www.youtube.com/watch?v=Kjj0HvCsqtA](https://www.youtube.com/watch?v=Kjj0HvCsqtA)

## 相關 Repository

這些 Repository 包含了後端應用和雲端部署相關的所有檔案：

- **前端 Repository：**[https://gitlab.com/music-share66/frontend.git](https://gitlab.com/music-share66/frontend.git)
- **後端 Repository**：[https://gitlab.com/music-share66/backend.git](https://gitlab.com/music-share66/backend.git)

# 資料夾結構

將資料夾分為 common、development、staging、production 四種主要資料夾。

- **common**：三環境通用資源例如VPC、Route 53、ACM、S3 等等。

- **development**：開發環境所需資源例如 RDS、ElastiCache、ECS 等等。

- **staging**：測試環境所需資源例如 RDS、ElastiCache、ECS、EKS 等等。

- **production**：正式環境所需資源例如 RDS、ElastiCache、ECS、EKS 等等。

# 建立雲端架構

使用 Terraform 在 AWS 上建立 development、staging、production 三種環境。

**Step 1. 建立共通資源**

使用以下指令進入到 common 資料夾：

```
cd deploy/common
```

新增 variables.auto.tfvars並根據 /common/variables.tf 內的值填入相對應的值。

使用以下指令初始化 Terraform：

```
terraform init
```

初始化完成後使用指令開始建立共通雲端基礎設施：

```
terraform apply
```

建立完成後可至 AWS consloe 檢查資源是否正確建立。

**Step 2. 建立對應環境資源**

進到對應環境的資料夾：

```
cd <environment>
```

新增 variables.auto.tfvars並根據 /common/variables.tf 內的值填入相對應的值。

使用以下指令初始化 Terraform：

```
terraform init
```

初始化完成後使用指令開始建立共通雲端基礎設施：

```
terraform apply
```

建立完成後可至 AWS consloe 檢查資源是否正確建立

# English Version

# Project Introduction

## Description

Music Share is a music sharing platform designed to allow users to easily upload, share, and stream music.

This is the cloud infrastructure repository for the project. For detailed project information, please refer to the README in the backend repository.

- **Backend Repository** : [https://gitlab.com/music-share66/backend.git](https://gitlab.com/music-share66/backend.git)

## Demo URL

You can view the demo of Music Share at the following URL : 

https://musicshare.liaojourney.com

**Note** :

- As there is no responsive web design (RWD), please open it on a display with a resolution of 1500x1180 or above for a better browsing experience.
- Available time: 09:00 GMT+8 - 18:00 GMT+8

## Demo Video

You can view the demo video of the Music Share platform at the following URL :

[https://www.youtube.com/watch?v=Kjj0HvCsqtA](https://www.youtube.com/watch?v=Kjj0HvCsqtA)

## Related Repositories

These repositories contain all files related to the frontend application and cloud deployment :

- **Frontend Repository** : [https://gitlab.com/music-share66/frontend.git](https://gitlab.com/music-share66/frontend.git)
- **Backend Repository** : [https://gitlab.com/music-share66/backend.git](https://gitlab.com/music-share66/backend.git)

# Directory Structure

The directories are divided into four main categories: common, development, staging, and production.

- **common** : Resources common to all three environments, such as VPC, Route 53, ACM, S3, etc.

- **development** : Resources needed for the development environment, such as RDS, ElastiCache, ECS, etc.

- **staging** : Resources needed for the testing environment, such as RDS, ElastiCache, ECS, EKS, etc.

- **production** : Resources needed for the production environment, such as RDS, ElastiCache, ECS, EKS, etc.

# Creating the Cloud Infrastructure

Use Terraform to create development, staging, and production environments on AWS.

**Step 1. Create Common Resources**

Navigate to the common folder using the following command :

```
cd deploy/common
```

Create a variables.auto.tfvars file and fill in the corresponding values based on the variables in /common/variables.tf.

Initialize Terraform using the following command :

```
terraform init
```

Once initialization is complete, create the common cloud infrastructure using the command :

```
terraform apply
```

After the creation is complete, check the AWS console to ensure that the resources have been correctly created.

**Step 2. Create Environment-specific Resources**

Navigate to the folder for the corresponding environment :

```
cd <environment>
```

Create a variables.auto.tfvars file and fill in the corresponding values based on the variables in /common/variables.tf.

Initialize Terraform using the following command :

```
terraform init
```

Once initialization is complete, create the environment-specific cloud infrastructure using the command :

```
terraform apply
```

After the creation is complete, check the AWS console to ensure that the resources have been correctly created.