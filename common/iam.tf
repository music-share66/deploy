# --- s3 access Policies ---
data "aws_iam_policy_document" "music_share_ecs_task_s3_access_doc" {
  statement {
    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:DeleteObject",
      "s3:ListBucket"
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.music_share.bucket}",
      "arn:aws:s3:::${aws_s3_bucket.music_share.bucket}/*"
    ]
    effect = "Allow"
  }
}

resource "aws_iam_policy" "music_share_ecs_task_s3_access_policy" {
  name        = "music-share-S3BucketAccessPolicy"
  description = "Policy for accessing S3 bucket for ECS tasks"
  policy      = data.aws_iam_policy_document.music_share_ecs_task_s3_access_doc.json
}
