# --- vpc ---
resource "aws_vpc" "music_share" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "vpc_music_share"
  }
}

# --- internet gateway ---
resource "aws_internet_gateway" "music_share" {
  vpc_id = aws_vpc.music_share.id

  tags = {
    Name = "igw_music_share"
  }
}

# --- security group ---
# all vpc in
resource "aws_security_group" "all_vpc_in" {
  name        = "all_vpc_in"
  description = "Allow all traffic from vpc internal"
  vpc_id      = aws_vpc.music_share.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [aws_vpc.music_share.cidr_block]
  }
}

# out
resource "aws_security_group" "all_out" {
  name        = "all_out"
  description = "Allow all traffic out"
  vpc_id      = aws_vpc.music_share.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ssh
resource "aws_security_group" "ssh" {
  name        = "ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.music_share.id

  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# http
resource "aws_security_group" "http" {
  name        = "http"
  description = "Allow HTTP inbound traffic"
  vpc_id      = aws_vpc.music_share.id

  ingress {
    description = "HTTP from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# https
resource "aws_security_group" "https" {
  name        = "https"
  description = "Allow HTTPS inbound traffic"
  vpc_id      = aws_vpc.music_share.id

  ingress {
    description = "HTTPS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# rds postgres
resource "aws_security_group" "postgres" {
  name        = "postgres"
  description = "Allow Postgres inbound traffic"
  vpc_id      = aws_vpc.music_share.id

  ingress {
    description = "Postgres from VPC"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# elastiCache redis
resource "aws_security_group" "redis" {
  name        = "redis"
  description = "Allow Redis inbound traffic"
  vpc_id      = aws_vpc.music_share.id

  ingress {
    description = "Redis from VPC"
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
