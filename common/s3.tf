resource "aws_s3_bucket" "music_share" {
  bucket        = "music-share"
  force_destroy = true
}

resource "aws_s3_bucket_public_access_block" "public" {
  bucket = aws_s3_bucket.music_share.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

data "aws_iam_policy_document" "s3_public_access" {
  statement {
    actions = ["s3:GetObject"]
    effect  = "Allow"

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    resources = [
      aws_s3_bucket.music_share.arn,
      "${aws_s3_bucket.music_share.arn}/*",
    ]
  }
}

resource "aws_s3_bucket_policy" "s3_public_access" {
  depends_on = [
    aws_s3_bucket.music_share,
    aws_s3_bucket_public_access_block.public
  ]

  bucket = aws_s3_bucket.music_share.id
  policy = data.aws_iam_policy_document.s3_public_access.json
}

resource "aws_s3_bucket_cors_configuration" "music_share_cors" {
  bucket = aws_s3_bucket.music_share.id

  cors_rule {
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
  }
}
