variable "DOMAIN_NAME" {
  description = "The domain name of the website."
  type        = string
}
