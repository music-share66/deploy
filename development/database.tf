# --- rds ---
# subnet group
resource "aws_db_subnet_group" "music_share_development" {
  name = "music_share_development_rds_subnet_group"

  subnet_ids = [
    aws_subnet.music_share_development_private_4.id,
    aws_subnet.music_share_development_private_5.id,
    aws_subnet.music_share_development_private_6.id,
  ]

  tags = {
    Name = "music_share_development_rds_subnet_group"
  }
}

# database postgres
resource "aws_db_instance" "music_share_development_postgres" {
  identifier           = "db-postgres-music-share-development"
  instance_class       = "db.t3.micro"
  engine               = "postgres"
  engine_version       = "15.4"
  username             = var.DB_USERNAME
  password             = var.DB_PASSWORD
  db_name              = var.DB_DATABASE
  allocated_storage    = 5
  db_subnet_group_name = aws_db_subnet_group.music_share_development.name
  port                 = "5432"
  vpc_security_group_ids = [
    data.aws_security_group.postgres.id,
    data.aws_security_group.all_out.id
  ]
  skip_final_snapshot = true
}
