# --- ecs ---
# cluster
resource "aws_ecs_cluster" "music_share_development" {
  name = "music_share_development"
}

# capacity provider
# frontend
resource "aws_ecs_capacity_provider" "music_share_development_frontend" {
  name = "cp_music_share_development_frontend"

  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.music_share_development_frontend.arn
    managed_termination_protection = "DISABLED"

    managed_scaling {
      maximum_scaling_step_size = 10000
      minimum_scaling_step_size = 1
      status                    = "ENABLED"
      target_capacity           = 100
    }
  }
}

# backend
resource "aws_ecs_capacity_provider" "music_share_development_backend" {
  name = "cp_music_share_development_backend"

  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.music_share_development_backend.arn
    managed_termination_protection = "DISABLED"

    managed_scaling {
      maximum_scaling_step_size = 10000
      minimum_scaling_step_size = 1
      status                    = "ENABLED"
      target_capacity           = 100
    }
  }
}

# cluster capacity provider
resource "aws_ecs_cluster_capacity_providers" "music_share_development" {
  cluster_name = aws_ecs_cluster.music_share_development.name
  capacity_providers = [
    aws_ecs_capacity_provider.music_share_development_frontend.name,
    aws_ecs_capacity_provider.music_share_development_backend.name
  ]
}

# aws ecs taskdefinition & service
# frontend task
resource "aws_ecs_task_definition" "music_share_development_frontend" {
  family             = "td_music_share_development_frontend"
  task_role_arn      = aws_iam_role.music_share_development_ecs_task_role.arn
  execution_role_arn = aws_iam_role.music_share_development_ecs_exec_role.arn
  network_mode       = "bridge"
  cpu                = 512
  memory             = 256

  container_definitions = jsonencode([{
    name         = "music_share_development_frontend",
    image        = "registry.gitlab.com/music-share66/frontend:dev-latest",
    essential    = true,
    portMappings = [{ containerPort = 80, hostPort = 80 }],
  }])
}

# frontend service
resource "aws_ecs_service" "music_share_development_frontend" {
  name                               = "svc_music_share_development_frontend"
  cluster                            = aws_ecs_cluster.music_share_development.id
  desired_count                      = var.ECS_FRONTEND_DESIRED_COUNT
  enable_ecs_managed_tags            = true
  enable_execute_command             = true
  scheduling_strategy                = "REPLICA"
  task_definition                    = aws_ecs_task_definition.music_share_development_frontend.arn
  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 0

  capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.music_share_development_frontend.name
    weight            = 1
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "attribute:ecs.availability-zone"
  }

  load_balancer {
    container_name   = "music_share_development_frontend"
    container_port   = 80
    target_group_arn = aws_lb_target_group.music_share_development_frontend.arn
  }
}

# backend task
resource "aws_ecs_task_definition" "music_share_development_backend" {
  family             = "td_music_share_development_backend"
  task_role_arn      = aws_iam_role.music_share_development_ecs_task_role.arn
  execution_role_arn = aws_iam_role.music_share_development_ecs_exec_role.arn
  network_mode       = "bridge"
  cpu                = 512
  memory             = 256

  container_definitions = jsonencode([{
    name         = "music_share_development_backend",
    image        = "registry.gitlab.com/music-share66/backend:dev-latest",
    essential    = true,
    portMappings = [{ containerPort = 3000, hostPort = 0 }],

    environment = [
      { name = "PORT", value = var.PORT },
      { name = "API_DOC_ENVIRONMENT", value = var.API_DOC_ENVIRONMENT },
      { name = "ENVIRONMENT", value = var.ENVIRONMENT },
      { name = "CLIENT_HOST", value = var.CLIENT_HOST },
      { name = "DB_TYPE", value = var.DB_TYPE },
      { name = "DB_HOST", value = var.DB_HOST },
      { name = "DB_PORT", value = var.DB_PORT },
      { name = "DB_USERNAME", value = var.DB_USERNAME },
      { name = "DB_PASSWORD", value = var.DB_PASSWORD },
      { name = "DB_DATABASE", value = var.DB_DATABASE },
      { name = "JWT_SECRET_KEY", value = var.JWT_SECRET_KEY },
      { name = "GOOGLE_CLIENT_ID", value = var.GOOGLE_CLIENT_ID },
      { name = "GOOGLE_CLIENT_SECRET", value = var.GOOGLE_CLIENT_SECRET },
      { name = "GOOGLE_OAUTH_URL", value = var.GOOGLE_OAUTH_URL },
      { name = "GOOGLE_CALL_BACK_URL", value = var.GOOGLE_CALL_BACK_URL },
      { name = "GOOGLE_ACCESS_TOKEN_URL", value = var.GOOGLE_ACCESS_TOKEN_URL },
      { name = "GOOGLE_TOKEN_INFO_URL", value = var.GOOGLE_TOKEN_INFO_URL },
      { name = "AWS_ACCESS_KEY", value = var.AWS_ACCESS_KEY },
      { name = "AWS_ACCESS_SECRET", value = var.AWS_ACCESS_SECRET },
      { name = "AWS_REGION", value = var.AWS_REGION },
      { name = "AWS_BUCKET_NAME", value = var.AWS_BUCKET_NAME },
      { name = "KAFKA_BROKER", value = var.KAFKA_BROKER },
      { name = "KAFKA_USERNAME", value = var.KAFKA_USERNAME },
      { name = "KAFKA_PASSWORD", value = var.KAFKA_PASSWORD },
      { name = "REDIS_HOST", value = var.REDIS_HOST },
    ]
  }])
}

# backend service
resource "aws_ecs_service" "music_share_development_backend" {
  name                               = "svc_music_share_development_backend"
  cluster                            = aws_ecs_cluster.music_share_development.id
  desired_count                      = var.ECS_BACKEND_DESIRED_COUNT
  enable_ecs_managed_tags            = true
  enable_execute_command             = true
  scheduling_strategy                = "REPLICA"
  task_definition                    = aws_ecs_task_definition.music_share_development_backend.arn
  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 0

  capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.music_share_development_backend.name
    weight            = 1
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "attribute:ecs.availability-zone"
  }

  load_balancer {
    container_name   = "music_share_development_backend"
    container_port   = 3000
    target_group_arn = aws_lb_target_group.music_share_development_backend.arn
  }
}
