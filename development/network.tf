# --- vpc ---
data "aws_vpc" "music_share" {
  tags = {
    Name = "vpc_music_share"
  }
}

# --- subnet ---
# public * 3 for web server
resource "aws_subnet" "music_share_development_public_1" {
  vpc_id                                      = data.aws_vpc.music_share.id
  availability_zone                           = "ap-northeast-1a"
  cidr_block                                  = "10.0.0.0/24"
  map_public_ip_on_launch                     = true
  enable_resource_name_dns_a_record_on_launch = true

  tags = {
    Name = "subnet_music_share_development_public_1"
  }
}

resource "aws_subnet" "music_share_development_public_2" {
  vpc_id                                      = data.aws_vpc.music_share.id
  availability_zone                           = "ap-northeast-1c"
  cidr_block                                  = "10.0.1.0/24"
  map_public_ip_on_launch                     = true
  enable_resource_name_dns_a_record_on_launch = true

  tags = {
    Name = "subnet_music_share_development_public_2"
  }
}

resource "aws_subnet" "music_share_development_public_3" {
  vpc_id                                      = data.aws_vpc.music_share.id
  availability_zone                           = "ap-northeast-1d"
  cidr_block                                  = "10.0.2.0/24"
  map_public_ip_on_launch                     = true
  enable_resource_name_dns_a_record_on_launch = true

  tags = {
    Name = "subnet_music_share_development_public_3"
  }
}

# private * 3 for appliaction server
resource "aws_subnet" "music_share_development_private_1" {
  vpc_id            = data.aws_vpc.music_share.id
  availability_zone = "ap-northeast-1a"
  cidr_block        = "10.0.3.0/24"

  tags = {
    Name = "subnet_music_share_development_private_1"
  }
}

resource "aws_subnet" "music_share_development_private_2" {
  vpc_id            = data.aws_vpc.music_share.id
  availability_zone = "ap-northeast-1c"
  cidr_block        = "10.0.4.0/24"

  tags = {
    Name = "subnet_music_share_development_private_2"
  }
}

resource "aws_subnet" "music_share_development_private_3" {
  vpc_id            = data.aws_vpc.music_share.id
  availability_zone = "ap-northeast-1d"
  cidr_block        = "10.0.5.0/24"

  tags = {
    Name = "subnet_music_share_development_private_3"
  }
}

# private * 3 for database server
resource "aws_subnet" "music_share_development_private_4" {
  vpc_id            = data.aws_vpc.music_share.id
  availability_zone = "ap-northeast-1a"
  cidr_block        = "10.0.6.0/24"

  tags = {
    Name = "subnet_music_share_development_private_4"
  }
}

resource "aws_subnet" "music_share_development_private_5" {
  vpc_id            = data.aws_vpc.music_share.id
  availability_zone = "ap-northeast-1c"
  cidr_block        = "10.0.7.0/24"

  tags = {
    Name = "subnet_music_share_development_private_5"
  }
}

resource "aws_subnet" "music_share_development_private_6" {
  vpc_id            = data.aws_vpc.music_share.id
  availability_zone = "ap-northeast-1d"
  cidr_block        = "10.0.8.0/24"

  tags = {
    Name = "subnet_music_share_development_private_6"
  }
}

# --- internet gateway ---
data "aws_internet_gateway" "music_share" {
  tags = {
    Name = "igw_music_share"
  }
}

# --- nat gateway ---
# elastic ip
resource "aws_eip" "music_share_development" {
  domain = "vpc"

  tags = {
    Name = "eip_music_share_development"
  }
}

# nat gateway
resource "aws_nat_gateway" "music_share_development" {
  allocation_id = aws_eip.music_share_development.id
  subnet_id     = aws_subnet.music_share_development_public_1.id

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [data.aws_internet_gateway.music_share]

  tags = {
    Name = "nat_music_share_development"
  }
}

# --- route table & association ---
# public route to internet gateway
resource "aws_route_table" "music_share_development_public_1" {
  vpc_id = data.aws_vpc.music_share.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = data.aws_internet_gateway.music_share.id
  }

  tags = {
    Name = "rt_music_share_development_public_1"
  }
}

resource "aws_route_table_association" "music_share_development_public_1" {
  subnet_id      = aws_subnet.music_share_development_public_1.id
  route_table_id = aws_route_table.music_share_development_public_1.id
}

resource "aws_route_table" "music_share_development_public_2" {
  vpc_id = data.aws_vpc.music_share.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = data.aws_internet_gateway.music_share.id
  }

  tags = {
    Name = "rt_music_share_development_public_2"
  }
}

resource "aws_route_table_association" "music_share_development_public_2" {
  subnet_id      = aws_subnet.music_share_development_public_2.id
  route_table_id = aws_route_table.music_share_development_public_2.id
}

resource "aws_route_table" "music_share_development_public_3" {
  vpc_id = data.aws_vpc.music_share.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = data.aws_internet_gateway.music_share.id
  }

  tags = {
    Name = "rt_music_share_development_public_3"
  }
}

resource "aws_route_table_association" "music_share_development_public_3" {
  subnet_id      = aws_subnet.music_share_development_public_3.id
  route_table_id = aws_route_table.music_share_development_public_3.id
}

# private for application server route to nat gateway
resource "aws_route_table" "music_share_development_private_1" {
  vpc_id = data.aws_vpc.music_share.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.music_share_development.id
  }

  tags = {
    Name = "rt_music_share_development_private_1"
  }
}

resource "aws_route_table_association" "music_share_development_private_1" {
  subnet_id      = aws_subnet.music_share_development_private_1.id
  route_table_id = aws_route_table.music_share_development_private_1.id
}

resource "aws_route_table" "music_share_development_private_2" {
  vpc_id = data.aws_vpc.music_share.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.music_share_development.id
  }

  tags = {
    Name = "rt_music_share_development_private_2"
  }
}

resource "aws_route_table_association" "music_share_development_private_2" {
  subnet_id      = aws_subnet.music_share_development_private_2.id
  route_table_id = aws_route_table.music_share_development_private_2.id
}

resource "aws_route_table" "music_share_development_private_3" {
  vpc_id = data.aws_vpc.music_share.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.music_share_development.id
  }

  tags = {
    Name = "rt_music_share_development_private_3"
  }
}

resource "aws_route_table_association" "music_share_development_private_3" {
  subnet_id      = aws_subnet.music_share_development_private_3.id
  route_table_id = aws_route_table.music_share_development_private_3.id
}

# --- security group ---
data "aws_security_group" "all_vpc_in" {
  vpc_id = data.aws_vpc.music_share.id
  name   = "all_vpc_in"
}

data "aws_security_group" "all_out" {
  vpc_id = data.aws_vpc.music_share.id
  name   = "all_out"
}

data "aws_security_group" "ssh" {
  vpc_id = data.aws_vpc.music_share.id
  name   = "ssh"
}

data "aws_security_group" "http" {
  vpc_id = data.aws_vpc.music_share.id
  name   = "http"
}

data "aws_security_group" "https" {
  vpc_id = data.aws_vpc.music_share.id
  name   = "https"
}

data "aws_security_group" "postgres" {
  vpc_id = data.aws_vpc.music_share.id
  name   = "postgres"
}

data "aws_security_group" "redis" {
  vpc_id = data.aws_vpc.music_share.id
  name   = "redis"
}

# --- vpc endpoint ---
resource "aws_vpc_endpoint" "music_share_development_ecs_agent" {
  vpc_id            = data.aws_vpc.music_share.id
  service_name      = "com.amazonaws.ap-northeast-1.ecs-agent"
  vpc_endpoint_type = "Interface"

  security_group_ids = [
    data.aws_security_group.all_vpc_in.id,
    data.aws_security_group.all_out.id
  ]

  subnet_ids = [
    aws_subnet.music_share_development_private_1.id,
    aws_subnet.music_share_development_private_2.id,
    aws_subnet.music_share_development_private_3.id,
  ]

  private_dns_enabled = true
}

resource "aws_vpc_endpoint" "music_share_development_ecs_telemetry" {
  vpc_id            = data.aws_vpc.music_share.id
  service_name      = "com.amazonaws.ap-northeast-1.ecs-telemetry"
  vpc_endpoint_type = "Interface"

  security_group_ids = [
    data.aws_security_group.all_vpc_in.id,
    data.aws_security_group.all_out.id
  ]

  subnet_ids = [
    aws_subnet.music_share_development_private_1.id,
    aws_subnet.music_share_development_private_2.id,
    aws_subnet.music_share_development_private_3.id,
  ]

  private_dns_enabled = true
}

resource "aws_vpc_endpoint" "music_share_development_ecs" {
  vpc_id            = data.aws_vpc.music_share.id
  service_name      = "com.amazonaws.ap-northeast-1.ecs"
  vpc_endpoint_type = "Interface"

  security_group_ids = [
    data.aws_security_group.all_vpc_in.id,
    data.aws_security_group.all_out.id
  ]

  subnet_ids = [
    aws_subnet.music_share_development_private_1.id,
    aws_subnet.music_share_development_private_2.id,
    aws_subnet.music_share_development_private_3.id,
  ]

  private_dns_enabled = true
}
