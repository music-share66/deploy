data "aws_route53_zone" "liaojourney_public" {
  name         = "liaojourney.com"
  private_zone = false
}

resource "aws_route53_record" "musicshare-development" {
  zone_id = data.aws_route53_zone.liaojourney_public.zone_id
  name    = "musicshare-dev.liaojourney.com"
  type    = "CNAME"
  ttl     = 300
  records = [aws_lb.music_share_development_frontend.dns_name]
}

resource "aws_route53_record" "musicshare-development-api" {
  zone_id = data.aws_route53_zone.liaojourney_public.zone_id
  name    = "musicshare-dev-api.liaojourney.com"
  type    = "CNAME"
  ttl     = 300
  records = [aws_lb.music_share_development_backend.dns_name]
}
