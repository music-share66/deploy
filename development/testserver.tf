# // compute
# resource "aws_instance" "testserver" {
#   ami           = "ami-012261b9035f8f938"
#   instance_type = "t2.micro"
#   subnet_id     = aws_subnet.music_share_development_public_1.id

#   vpc_security_group_ids = [
#     data.aws_security_group.ssh.id,
#     data.aws_security_group.http.id,
#     data.aws_security_group.all_out.id
#   ]

#   user_data = <<-EOF
#         #!/bin/bash
#         # Use this for your user data (script from top to bottom)
#         yum update -y
#         yum install -y httpd
#         systemctl start httpd
#         systemctl enable httpd
#         echo "<h1>Hello World from \$(hostname -f)</h1>" > /var/www/html/index.html
#     EOF
# }

# // route53
# data "aws_route53_zone" "liaojourney_public" {
#   name         = "liaojourney.com"
#   private_zone = false
# }

# resource "aws_route53_record" "musicshare-dev-api" {
#   depends_on = [aws_instance.testserver]
#   zone_id    = data.aws_route53_zone.liaojourney_public.zone_id
#   name       = "musicshare-dev-api.liaojourney.com"
#   type       = "A"
#   ttl        = 300
#   records    = [aws_instance.testserver.public_ip]
# }

# resource "aws_route53_record" "musicshare-stg-api" {
#   depends_on = [aws_instance.testserver]
#   zone_id    = data.aws_route53_zone.liaojourney_public.zone_id
#   name       = "musicshare-stg-api.liaojourney.com"
#   type       = "A"
#   ttl        = 300
#   records    = [aws_instance.testserver.public_ip]
# }

# resource "aws_route53_record" "musicshare-prod-api" {
#   depends_on = [aws_instance.testserver]
#   zone_id    = data.aws_route53_zone.liaojourney_public.zone_id
#   name       = "musicshare-api.liaojourney.com"
#   type       = "A"
#   ttl        = 300
#   records    = [aws_instance.testserver.public_ip]
# }
