# ecs server count
variable "ECS_FRONTEND_DESIRED_COUNT" {
  description = "The desired count of the frontend ECS service."
  type        = number
}

variable "ECS_BACKEND_DESIRED_COUNT" {
  description = "The desired count of the backend ECS service."
  type        = number
}

# ecs backend task definition variables
variable "PORT" {
  description = "The port number on which the server will listen."
  type        = string
}

variable "API_DOC_ENVIRONMENT" {
  description = "The environment for the API documentation (e.g., production, staging)."
  type        = string
}

variable "ENVIRONMENT" {
  description = "The deployment environment (e.g., development, staging, production)."
  type        = string
}

variable "CLIENT_HOST" {
  description = "The hostname or IP address of the client."
  type        = string
}

variable "DB_TYPE" {
  description = "The type of database being used (e.g., mysql, postgresql)."
  type        = string
}

variable "DB_HOST" {
  description = "The hostname or IP address of the database server."
  type        = string
}

variable "DB_PORT" {
  description = "The port number on which the database server is listening."
  type        = string
}

variable "DB_USERNAME" {
  description = "The username for connecting to the database."
  type        = string
}

variable "DB_PASSWORD" {
  description = "The password for connecting to the database."
  type        = string
}

variable "DB_DATABASE" {
  description = "The name of the database to connect to."
  type        = string
}

variable "JWT_SECRET_KEY" {
  description = "The secret key used for signing JWT tokens."
  type        = string
}

variable "GOOGLE_CLIENT_ID" {
  description = "The client ID for Google OAuth."
  type        = string
}

variable "GOOGLE_CLIENT_SECRET" {
  description = "The client secret for Google OAuth."
  type        = string
}

variable "GOOGLE_OAUTH_URL" {
  description = "The URL for initiating Google OAuth."
  type        = string
}

variable "GOOGLE_CALL_BACK_URL" {
  description = "The callback URL for Google OAuth."
  type        = string
}

variable "GOOGLE_ACCESS_TOKEN_URL" {
  description = "The URL for obtaining the Google OAuth access token."
  type        = string
}

variable "GOOGLE_TOKEN_INFO_URL" {
  description = "The URL for obtaining Google token information."
  type        = string
}

variable "AWS_ACCESS_KEY" {
  description = "The AWS access key for accessing AWS services."
  type        = string
}

variable "AWS_ACCESS_SECRET" {
  description = "The AWS access secret for accessing AWS services."
  type        = string
}

variable "AWS_REGION" {
  description = "The AWS region where the services are hosted."
  type        = string
}

variable "AWS_BUCKET_NAME" {
  description = "The name of the AWS S3 bucket."
  type        = string
}

variable "KAFKA_BROKER" {
  description = "The address of the Kafka broker."
  type        = string
}

variable "KAFKA_USERNAME" {
  description = "The username for connecting to Kafka."
  type        = string
}

variable "KAFKA_PASSWORD" {
  description = "The password for connecting to Kafka."
  type        = string
}

variable "REDIS_HOST" {
  description = "The hostname or IP address of the Redis server."
  type        = string
}
