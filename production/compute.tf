# --- ami ---
data "aws_ssm_parameter" "ecs_node_ami" {
  name = "/aws/service/ecs/optimized-ami/amazon-linux-2/recommended/image_id"
}

# --- lunch template ---
resource "aws_launch_template" "music_share_production" {
  name          = "lt_music_share_production"
  image_id      = data.aws_ssm_parameter.ecs_node_ami.value
  instance_type = "t2.micro"
  vpc_security_group_ids = [
    data.aws_security_group.all_vpc_in.id,
    data.aws_security_group.all_out.id,
    data.aws_security_group.ssh.id,
    data.aws_security_group.http.id
  ]

  iam_instance_profile {
    arn = aws_iam_instance_profile.ecs_node.arn
  }

  metadata_options {
    http_endpoint               = "enabled"
    http_tokens                 = "required"
    http_put_response_hop_limit = 10
    instance_metadata_tags      = "enabled"
  }

  user_data = base64encode(<<-EOF
    #!/bin/bash
    echo ECS_CLUSTER=${aws_ecs_cluster.music_share_production.name} >> /etc/ecs/ecs.config;
    EOF
  )
}

# --- auto scaling group ---
resource "aws_autoscaling_group" "music_share_production_frontend" {
  name             = "asg_music_share_production_frontend"
  desired_capacity = 0
  min_size         = 0
  max_size         = 1
  vpc_zone_identifier = [
    aws_subnet.music_share_production_public_1.id,
    aws_subnet.music_share_production_public_2.id,
    aws_subnet.music_share_production_public_3.id,
  ]
  health_check_type     = "EC2"
  protect_from_scale_in = false
  force_delete          = true

  launch_template {
    id      = aws_launch_template.music_share_production.id
    version = "$Latest"
  }

  tag {
    key                 = "AmazonECSManaged"
    value               = true
    propagate_at_launch = true
  }

  tag {
    key                 = "music_share_production"
    value               = "frontend"
    propagate_at_launch = true
  }
}

# --- load balancer ---
# alb
resource "aws_lb" "music_share_production_frontend" {
  desync_mitigation_mode           = "defensive"
  enable_cross_zone_load_balancing = true
  internal                         = false
  ip_address_type                  = "ipv4"
  load_balancer_type               = "application"
  name                             = "alb-music-share-prod-frontend"
  security_groups = [
    data.aws_security_group.http.id,
    data.aws_security_group.https.id,
    data.aws_security_group.all_out.id
  ]
  subnets = [
    aws_subnet.music_share_production_public_1.id,
    aws_subnet.music_share_production_public_2.id,
    aws_subnet.music_share_production_public_3.id,
  ]
}

# target group
resource "aws_lb_target_group" "music_share_production_frontend" {
  deregistration_delay              = "60"
  ip_address_type                   = "ipv4"
  load_balancing_algorithm_type     = "round_robin"
  load_balancing_cross_zone_enabled = "use_load_balancer_configuration"
  name                              = "alb-tg-music-share-prod-frontend"
  port                              = 80
  protocol                          = "HTTP"
  protocol_version                  = "HTTP1"
  slow_start                        = 0
  target_type                       = "instance"
  vpc_id                            = data.aws_vpc.music_share.id

  health_check {
    enabled             = true
    healthy_threshold   = 5
    interval            = 30
    matcher             = "200"
    path                = "/health"
    port                = "traffic-port"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 2
  }
}

# listener
resource "aws_lb_listener" "music_share_production_frontend_http" {
  load_balancer_arn = aws_lb.music_share_production_frontend.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "music_share_production_frontend_https" {
  certificate_arn   = data.aws_acm_certificate.liaojourney.arn
  load_balancer_arn = aws_lb.music_share_production_frontend.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"

  default_action {
    target_group_arn = aws_lb_target_group.music_share_production_frontend.arn
    type             = "forward"
  }
}
