data "aws_route53_zone" "liaojourney_public" {
  name         = "liaojourney.com"
  private_zone = false
}

resource "aws_route53_record" "musicshare-production" {
  zone_id = data.aws_route53_zone.liaojourney_public.zone_id
  name    = "musicshare.liaojourney.com"
  type    = "CNAME"
  ttl     = 300
  records = [aws_lb.music_share_production_frontend.dns_name]
}
