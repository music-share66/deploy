# ecs server count
variable "ECS_FRONTEND_DESIRED_COUNT" {
  description = "The desired count of the frontend ECS service."
  type        = number
}

# database information
variable "DB_USERNAME" {
  description = "The username for connecting to the database."
  type        = string
}

variable "DB_PASSWORD" {
  description = "The password for connecting to the database."
  type        = string
}

variable "DB_DATABASE" {
  description = "The name of the database to connect to."
  type        = string
}