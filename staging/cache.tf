# --- elastiCache ---
# subnet group
resource "aws_elasticache_subnet_group" "music_share_staging" {
  name = "music-share-staging-elasticache-subnet-group"

  subnet_ids = [
    aws_subnet.music_share_staging_private_4.id,
    aws_subnet.music_share_staging_private_5.id,
    aws_subnet.music_share_staging_private_6.id,
  ]

  tags = {
    Name = "music_share_staging_elasticache_subnet_group"
  }
}

# cache redis
resource "aws_elasticache_replication_group" "music_share_staging_redis" {
  apply_immediately           = null
  description                 = "music share cache redis for staging environment"
  at_rest_encryption_enabled  = false
  auth_token                  = null # sensitive
  auth_token_update_strategy  = null
  auto_minor_version_upgrade  = true
  automatic_failover_enabled  = false
  data_tiering_enabled        = false
  engine                      = "redis"
  engine_version              = "7.1"
  final_snapshot_identifier   = null
  global_replication_group_id = null
  ip_discovery                = "ipv4"
  kms_key_id                  = null
  maintenance_window          = "sun:15:30-sun:16:30"
  multi_az_enabled            = false
  network_type                = "ipv4"
  node_type                   = "cache.t2.micro"
  notification_topic_arn      = null
  num_node_groups             = 1
  parameter_group_name        = "default.redis7"
  port                        = 6379
  preferred_cache_cluster_azs = null
  replicas_per_node_group     = 0
  replication_group_id        = "music-share-staging-redis"
  security_group_ids = [
    data.aws_security_group.redis.id,
  ]
  security_group_names       = null
  snapshot_arns              = null
  snapshot_name              = null
  snapshot_retention_limit   = 0
  snapshot_window            = "19:00-20:00"
  subnet_group_name          = aws_elasticache_subnet_group.music_share_staging.name
  tags                       = {}
  tags_all                   = {}
  transit_encryption_enabled = false
}
