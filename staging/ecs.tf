# --- ecs ---
# cluster
resource "aws_ecs_cluster" "music_share_staging" {
  name = "music_share_staging"
}

# capacity provider
# frontend
resource "aws_ecs_capacity_provider" "music_share_staging_frontend" {
  name = "cp_music_share_staging_frontend"

  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.music_share_staging_frontend.arn
    managed_termination_protection = "DISABLED"

    managed_scaling {
      maximum_scaling_step_size = 10000
      minimum_scaling_step_size = 1
      status                    = "ENABLED"
      target_capacity           = 100
    }
  }
}

# cluster capacity provider
resource "aws_ecs_cluster_capacity_providers" "music_share_staging" {
  cluster_name = aws_ecs_cluster.music_share_staging.name
  capacity_providers = [
    aws_ecs_capacity_provider.music_share_staging_frontend.name
  ]
}

# aws ecs taskdefinition & service
# frontend task
resource "aws_ecs_task_definition" "music_share_staging_frontend" {
  family             = "td_music_share_staging_frontend"
  task_role_arn      = aws_iam_role.music_share_staging_ecs_task_role.arn
  execution_role_arn = aws_iam_role.music_share_staging_ecs_exec_role.arn
  network_mode       = "bridge"
  cpu                = 512
  memory             = 256

  container_definitions = jsonencode([{
    name         = "music_share_staging_frontend",
    image        = "registry.gitlab.com/music-share66/frontend:stg-latest",
    essential    = true,
    portMappings = [{ containerPort = 80, hostPort = 80 }],
  }])
}

# frontend service
resource "aws_ecs_service" "music_share_staging_frontend" {
  name                               = "svc_music_share_staging_frontend"
  cluster                            = aws_ecs_cluster.music_share_staging.id
  desired_count                      = var.ECS_FRONTEND_DESIRED_COUNT
  enable_ecs_managed_tags            = true
  enable_execute_command             = true
  scheduling_strategy                = "REPLICA"
  task_definition                    = aws_ecs_task_definition.music_share_staging_frontend.arn
  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 0

  capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.music_share_staging_frontend.name
    weight            = 1
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "attribute:ecs.availability-zone"
  }

  load_balancer {
    container_name   = "music_share_staging_frontend"
    container_port   = 80
    target_group_arn = aws_lb_target_group.music_share_staging_frontend.arn
  }
}
