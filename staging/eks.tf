module "music_share_staging_eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 20.0"

  cluster_name    = "music-share-staging"
  cluster_version = "1.29"

  cluster_endpoint_public_access = true

  vpc_id = data.aws_vpc.music_share.id

  control_plane_subnet_ids = [
    resource.aws_subnet.music_share_staging_public_1.id,
    resource.aws_subnet.music_share_staging_public_2.id,
    resource.aws_subnet.music_share_staging_public_3.id
  ]

  subnet_ids = [
    resource.aws_subnet.music_share_staging_private_1.id,
    resource.aws_subnet.music_share_staging_private_2.id,
    resource.aws_subnet.music_share_staging_private_3.id
  ]

  enable_cluster_creator_admin_permissions = true
  enable_irsa                              = true

  eks_managed_node_groups = {
    general = {
      min_size     = 1
      max_size     = 2
      desired_size = 2

      labels = {
        role = "general"
      }

      instance_types = ["t3.small"]
      capacity_type  = "ON_DEMAND"

      create_iam_role          = true
      iam_role_name            = "staging-eks-access-s3-mediaconvert"
      iam_role_use_name_prefix = false
      iam_role_description     = "Staging EKS managed node group access to s3 and mediaconvert"
      iam_role_tags = {
        Purpose = "Staging EKS managed node group access to s3 and mediaconvert"
      }
      iam_role_additional_policies = {
        StagingEksAccessS3           = data.aws_iam_policy.music_share_ecs_task_s3_access_policy.arn
        StagingEksAccessMediaConvert = data.aws_iam_policy.music_share_mediaconvert_access_policy.arn
      }
    }
  }
}

data "aws_eks_cluster_auth" "music_share_staging" {
  depends_on = [module.music_share_staging_eks]
  name       = module.music_share_staging_eks.cluster_name
}

provider "helm" {
  kubernetes {
    host                   = module.music_share_staging_eks.cluster_endpoint
    cluster_ca_certificate = base64decode(module.music_share_staging_eks.cluster_certificate_authority_data)
    token                  = data.aws_eks_cluster_auth.music_share_staging.token
  }
}

module "music_share_staging_eks_aws_load_balancer_controller_irsa_role" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
  version = "5.37.1"

  role_name = "staging-aws-load-balancer-controller"

  attach_load_balancer_controller_policy = true

  oidc_providers = {
    ex = {
      provider_arn               = module.music_share_staging_eks.oidc_provider_arn
      namespace_service_accounts = ["kube-system:staging-aws-load-balancer-controller"]
    }
  }
}

resource "helm_release" "music_share_staging_eks_aws_load_balancer_controller" {
  depends_on = [module.music_share_staging_eks]

  name = "aws-load-balancer-controller"

  repository = "https://aws.github.io/eks-charts"
  chart      = "aws-load-balancer-controller"
  namespace  = "kube-system"
  version    = "1.7.1"

  set {
    name  = "serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
    value = module.music_share_staging_eks_aws_load_balancer_controller_irsa_role.iam_role_arn
  }

  set {
    name  = "serviceAccount.name"
    value = "staging-aws-load-balancer-controller"
  }

  set {
    name  = "replicaCount"
    value = 1
  }

  set {
    name  = "clusterName"
    value = module.music_share_staging_eks.cluster_name
  }
}

module "music_share_staging_eks_external_dns_irsa_role" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
  version = "5.37.1"

  role_name = "staging-external-dns"

  attach_external_dns_policy    = true
  external_dns_hosted_zone_arns = ["arn:aws:route53:::hostedzone/${data.aws_route53_zone.liaojourney_public.zone_id}"]

  oidc_providers = {
    ex = {
      provider_arn               = module.music_share_staging_eks.oidc_provider_arn
      namespace_service_accounts = ["kube-system:staging-external-dns"]
    }
  }
}

resource "helm_release" "external-dns" {
  depends_on = [helm_release.music_share_staging_eks_aws_load_balancer_controller]

  name = "external-dns"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "external-dns"
  namespace  = "kube-system"
  version    = "7.1.0"

  set {
    name  = "serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
    value = module.music_share_staging_eks_external_dns_irsa_role.iam_role_arn
  }

  set {
    name  = "serviceAccount.name"
    value = "staging-external-dns"
  }

  set {
    name  = "zoneType"
    value = "public"
  }

  set {
    name  = "policy"
    value = "sync"
  }

  set {
    name  = "domainFilters[0]"
    value = "liaojourney.com"
  }

  set {
    name  = "provider"
    value = "aws"
  }

  set {
    name  = "txtOwnerId" #TXT record identifier
    value = "eks-staging"
  }
}
