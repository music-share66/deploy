# --- ECS Node Role ---
data "aws_iam_policy_document" "ecs_node_doc" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "music_share_staging_ecs_node_role" {
  name_prefix        = "music-share-staging-ecs-node-role"
  assume_role_policy = data.aws_iam_policy_document.ecs_node_doc.json
}

resource "aws_iam_role_policy_attachment" "music_share_staging_ecs_node_role_policy" {
  role       = aws_iam_role.music_share_staging_ecs_node_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "ecs_node" {
  name_prefix = "music-share-staging-ecs-node-profile"
  path        = "/ecs/instance/"
  role        = aws_iam_role.music_share_staging_ecs_node_role.name
}

# --- ECS Task Role ---
data "aws_iam_policy_document" "ecs_task_doc" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "music_share_staging_ecs_task_role" {
  name_prefix        = "music-share-staging-ecs-task-role"
  assume_role_policy = data.aws_iam_policy_document.ecs_task_doc.json
}

resource "aws_iam_role" "music_share_staging_ecs_exec_role" {
  name_prefix        = "music-share-staging-ecs-exec-role"
  assume_role_policy = data.aws_iam_policy_document.ecs_task_doc.json
}

resource "aws_iam_role_policy_attachment" "music_share_staging_ecs_exec_role_policy" {
  role       = aws_iam_role.music_share_staging_ecs_exec_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

data "aws_iam_policy" "music_share_ecs_task_s3_access_policy" {
  name = "music-share-S3BucketAccessPolicy"
}

resource "aws_iam_role_policy_attachment" "music_share_staging_ecs_task_role_s3_access_policy" {
  role       = aws_iam_role.music_share_staging_ecs_task_role.name
  policy_arn = data.aws_iam_policy.music_share_ecs_task_s3_access_policy.arn
}

data "aws_iam_policy" "music_share_mediaconvert_access_policy" {
  name = "AWSElementalMediaConvertFullAccess"
}
