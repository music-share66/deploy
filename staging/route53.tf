data "aws_route53_zone" "liaojourney_public" {
  name         = "liaojourney.com"
  private_zone = false
}

resource "aws_route53_record" "musicshare-staging" {
  zone_id = data.aws_route53_zone.liaojourney_public.zone_id
  name    = "musicshare-stg.liaojourney.com"
  type    = "CNAME"
  ttl     = 300
  records = [aws_lb.music_share_staging_frontend.dns_name]
}
